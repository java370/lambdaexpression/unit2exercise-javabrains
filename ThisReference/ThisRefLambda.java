public class ThisRefLambda {
    public void doProcess(int i, Process process) {
        process.Process(i);
    }

    public void helperFunc(int v) {
        doProcess(v, (x) -> {
            System.out.println(x);
            System.out.println(this);
        });
    }

    public static void main(String[] args) {
        ThisRefLambda ref = new ThisRefLambda();
        int v = 88;
        ref.helperFunc(v);
    }

    @Override
    public String toString() {
        return "ThisRefLambda";
    }
}
