public class ThisRef {
    public void doProcess(int i, Process process) {
        process.Process(i);
    }

    public static void main(String[] args) {
        ThisRef ref = new ThisRef();
        int v = 88;
        ref.doProcess(v, new Process() {
            @Override
            public void Process(int x) {
                System.out.println(x);
                System.out.println(this);
            }

            public String toString() {
                return "Process";
            }
        });
    }

    // not working
    public String toString() {
        return "ThisRef";
    }
}