public class ClosureEx {
    public static void main(String[] args) {
        int m = 20;
        // Lambda
        doProcess(m, h -> System.out.println(h + m));

        // not using Lambda
        doProcess(m, new Process() {
            public void Process(int p){
                System.out.println(p);
            }
        });
    }

    public static void doProcess(int i, Process process) {
        process.Process(i);
    }
}