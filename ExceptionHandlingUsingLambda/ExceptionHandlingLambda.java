import java.util.function.BiConsumer;

public class ExceptionHandlingLambda {
    public static void main(String[] args) {
        int[] nums = { 2, 4, 6, 8 };

        int key = 2;
        // multiplication
        process(nums, key, (p, q) -> System.out.println(p * q));
        // division
        process(nums, key, (p, q) -> {
            try {
                System.out.println(p / q);
            } catch (ArithmeticException e) {
                System.out.println("Can't divide by Zero!");
            }
        });
    }

    private static void process(int[] nums, int key, BiConsumer<Integer, Integer> biConsumer) {
        for (int i : nums) {
            biConsumer.accept(i, key);
        }
    }
}