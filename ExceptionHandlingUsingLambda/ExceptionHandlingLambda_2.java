import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ExceptionHandlingLambda_2 {
    public static void main(String[] args) {
        int[] nums = { 2, 4, 6, 8 };

        int key = 0;

        // Simplified division operation with try-catch block
        process(nums, key, passThroughFunc((p, q) -> System.out.println(p / q)));
    }

    private static void process(int[] nums, int key, BiConsumer<Integer, Integer> biConsumer) {
        for (int i : nums) {
            biConsumer.accept(i, key);
        }
    }

    private static BiConsumer<Integer, Integer> passThroughFunc(BiConsumer<Integer, Integer> biConsumer) {
        return (p, q) -> {
            try {
                biConsumer.accept(p, q);
            } catch (ArithmeticException e) {
                System.out.println("Can't divide by Zero!");
            }
        };
    }
}
