import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ConsumerInterfaceTest {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles", "Dickens", 60),
                new Person("Lewis", "Carroll", 42),
                new Person("Thomas", "Carlyle", 51),
                new Person("Charlotte", "Bronte", 45),
                new Person("Matthew", "Arnold", 39));

        // Step 1: Sort list by last name
        Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

        // Step 2: Create a method that prints all elements in the list
        conditionalPrinting(people, p -> true, p -> System.out.println(p));
        // Step 3: Create a method that prints age of all people that have last name
        // beginning with C

        conditionalPrinting(people, (p) -> p.getLastName().startsWith("C"), p -> System.out.println(p.getAge()));

    }

    // implemented predicate interface to avoid Condition interface implemntation
    private static void conditionalPrinting(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
        for (Person person : people) {
            if (predicate.test(person)) {
                consumer.accept(person);
            }
        }
    }
}
